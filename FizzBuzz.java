public class FizzBuzz {

    public static String fizbuzz(int n){
        if (n % 15 == 0){
            return "FizzBuzz";
        }
        else if (n % 3 == 0){
            return "Fizz";
        }
        else if (n % 5 == 0){
            return "Buzz";
        }
        else{
            return "Nothing";
        }
    }

    public static void main(String[] args) {
        System.out.println(fizbuzz(20)); // Output: Fizz
        System.out.println(fizbuzz(5)); // Output: Buzz
        System.out.println(fizbuzz(30)); // Output: FizzBuzz
        System.out.println(fizbuzz(7)); // Output: Nothing
    }
}
