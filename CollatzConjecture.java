import java.util.List;
import java.util.ArrayList;

public class CollatzConjecture {
    // Function to generate the Collatz sequence for a given number
    public static List<Integer> generateCollatzSequence(int n) {
        List<Integer> sequence = new ArrayList<>();
        while (n != 1) {
            sequence.add(n);
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = (3 * n) + 1;
            }
        }
        sequence.add(1); // Add the final number (1) to the sequence
        return sequence;
    }

    // Function to print the Collatz sequence
    public static void printCollatzSequence(List<Integer> sequence) {
        for (int num : sequence) {
            System.out.print(num + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int startingNumber = 27; // Example: Start the sequence with 27
        List<Integer> collatzSequence = generateCollatzSequence(startingNumber);
        printCollatzSequence(collatzSequence);
    }
}
