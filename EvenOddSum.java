public class EvenOddSum {

    public static int sumOfEven(int n){
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        return sum;
    }

    // public static int sumOfOdd(int n){
    //     int sum = 0;
    //     for (int i = 0; i <= n; i++) {
    //         if (i % 2 != 0) {
    //             sum += i;
    //         }
    //     }
    //     return sum;
    // }

    public static void main(String[] args) {
        int limit = 25; // Example: Sum of numbers up to 25
        int sumEven = sumOfEven(limit);
        // int sumOdd = sumOfOdd(limit);
        System.out.println("Sum of even numbers: " + sumEven);
        // System.out.println("Sum of odd numbers: " + sumOdd);
    }
    
        
    }

