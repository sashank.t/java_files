public class SumOfDigits {
//    public static void main(String[] args) {
//     String S = "123";
//     int num = Integer.parseInt(S);
//     System.out.println(num);
//    }
public static int parsing(String S){
    return Integer.parseInt(S);
}
public static int sum(int num ){
    int sum = 0;
    while(num > 0){
        sum += num % 10;
        num /= 10;
    }
return sum;
}
public static void main(String[] args) {
    String S = "123";
    int num = parsing(S);
    int sum = sum(num);
    System.out.println(sum);
}

}

