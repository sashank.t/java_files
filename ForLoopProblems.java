public class ForLoopProblems {
    public static int looper(int n){
        for (int i = 0; i <= n; i++){
            if(i % 2 == 0){
                return i; // Return the first even number encountered
            }
        }
        return n; // Return n if no even number is found
    }

    public static void main(String[] args) {
        System.out.println(looper(2)); // Output: 0 (first even number encountered)
    }
}
